﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DinoGameChampion
{
    public partial class Form1 : Form
    {
        const float BRIGHTNESS_RANGE_THRESHOLD = 0.2f;
        const int JUMP_MILLISECONDS = 150; // Empirically 150
        const int JUMP_POINT = 920;
        const float JUMP_DELAY_TRANSFORM_SLOPE = 0.029722f;
        const float JUMP_DELAY_TRANSFORM_INTERCEPT = -300.6667f;

        private Screen screen;
        private delegate void ObstacleEventHandler(DateTime a, DateTime b);

        private readonly Rectangle triggerArea1 = new Rectangle(1200, 225, 40, 30);
        private readonly Rectangle triggerArea2 = new Rectangle(1050, 225, 40, 30);
        private readonly Rectangle timeOfDayArea = new Rectangle(500, 100, 2, 2);
        private readonly Rectangle gameOverArea = new Rectangle(943, 203, 35, 32);
        private readonly Rectangle gameOverG = new Rectangle(862, 170, 15, 12);
        private readonly Rectangle gameOverA = new Rectangle(889, 170, 15, 12);
        private readonly Rectangle gameOverM = new Rectangle(913, 170, 15, 12);
        private readonly Rectangle gameOverE = new Rectangle(937, 170, 15, 12);


        private bool trigger1On = false;
        private bool trigger2On = false;
        private Queue<DateTime> obstacleAppearTimes = new Queue<DateTime>();
        private Queue<DateTime> jumpTimes = new Queue<DateTime>();
        private ObstacleEventHandler obstacleEvent;
        private int triggerDistance;
        private int jumpDistance;

        //                                                  Daytime:
        // Dino location:           (660, 220, 45, 50)      Brightness: 0.7477279
        // Danger strip:            (660, 225, 600, 30)
        // Night time indicator:    (500, 100, 2, 2)        Brightness: 0.9686275
        // Danger Zone 1:           (1200, 225, 40, 30)
        // Danger Zone 2:           (900, 225, 40, 30)
        // Whole scan strip:        (705, 225, 600, 30)     Brightness: 0.9687069
        // Game Over box            (943, 203, 35, 32)      Brightness: 0.4799618

        // One Big Cactus Full: 0.7478199
        // One small Cactus Full: 0.8689431
        // Top bird: 0.91

        // Night:
        // Background: 0
        // Small cactus: 0.1

        public Form1()
        {
            InitializeComponent();
        }

        // Get a handle to an application window.
        [DllImport("USER32.DLL", CharSet = CharSet.Unicode)]
        public static extern IntPtr FindWindow(string lpClassName,
            string lpWindowName);

        // Activate an application window.
        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        private void playButton_Click(object sender, EventArgs e)
        {
            screen = Screen.FromControl(this);
            FocusChrome();


            //Bitmap image = CaptureImage(937, 170, 15, 12);
            //image.Save(@"D:\Jake\Projects\DinoGameChampion\Images\000test.png", ImageFormat.Png);


            triggerDistance = (triggerArea1.Left + triggerArea1.Width) - (triggerArea2.Left + triggerArea2.Width);
            jumpDistance = (triggerArea2.Left + triggerArea2.Width) - JUMP_POINT;
            obstacleEvent += HandleObstacle;

            Jump(); // Start Game
            Thread.Sleep(500);

            Thread captureThread = new Thread(CaptureLoop);
            captureThread.Start();

            Thread gameOverThread = new Thread(DetectGameOver);
            gameOverThread.Start();

            while (true)
            {
                if (jumpTimes.Count == 0) continue;

                DateTime jumpTime = jumpTimes.Dequeue();
                int millis = (jumpTime - DateTime.Now).Milliseconds;
                //Console.WriteLine("Jump in " + millis);
                if (millis > 0)
                {
                    Thread.Sleep(millis);
                }
                Jump();
                Thread.Sleep(JUMP_MILLISECONDS);
            }

        }

        private void CaptureLoop()
        { 
            while (true)
            {
                DetectArea1();
                DetectArea2();
            }
        }

        private void DetectGameOver()
        {
            Rectangle[] letters = { gameOverG, gameOverA, gameOverM, gameOverE };
            bool gameOver;
            float range = 0;

            while (true)
            {
                gameOver = true;

                for (int i = 0; i < letters.Length; i++)
                {
                    Bitmap image = CaptureBox(letters[i]);
                    range = GetBrightnessRange(image);
                    if (range < BRIGHTNESS_RANGE_THRESHOLD)
                    {
                        gameOver = false;
                        break;
                    }
                }

                if (gameOver)
                {
                    GameOver();
                    break;
                }

                Thread.Sleep(3000);
            }
        }

        private void GameOver()
        {
            Console.WriteLine("Game over.");
        }

        private void HandleObstacle(DateTime time1, DateTime time2)
        {
            float timePerDistance = (time2.Ticks - time1.Ticks) / (float)triggerDistance;
            jumpTimes.Enqueue(DateTime.Now + new TimeSpan(JumpDelayTransform(timePerDistance)));
        }

        private long JumpDelayTransform(float ticksPerPixel)
        {
            return (long)((JUMP_DELAY_TRANSFORM_SLOPE * ticksPerPixel + JUMP_DELAY_TRANSFORM_INTERCEPT) * 10000);
        }

        private void DetectArea1()
        {
            DateTime captureTime = DateTime.Now;
            Bitmap image = CaptureBox(triggerArea1);
            float range = GetBrightnessRange(image);
            bool obstacle = range > BRIGHTNESS_RANGE_THRESHOLD;
            //bool obstacle = ContainsObstacle(triggerArea1);

            if (obstacle)
            {
                //Console.WriteLine("                           * Range: " + range);
                trigger1On = true;
            }
            else
            {
                //Console.WriteLine("                             Range: " + range);
                if (trigger1On)
                {
                    //Console.WriteLine("                             End");
                    obstacleAppearTimes.Enqueue(captureTime);
                }

                trigger1On = false;
            }
        }

        private void DetectArea2()
        {
            if (obstacleAppearTimes.Count == 0) return;

            DateTime captureTime = DateTime.Now;
            Bitmap image = CaptureBox(triggerArea2);
            float range = GetBrightnessRange(image);
            bool obstacle = range > BRIGHTNESS_RANGE_THRESHOLD;
            //bool obstacle = ContainsObstacle(triggerArea1);

            if (obstacle)
            {
                //Console.WriteLine("     * Range: " + range);
                trigger2On = true;
            }
            else
            {
                //Console.WriteLine("       Range: " + range);
                if (trigger2On)
                {
                    //Console.WriteLine("       End");
                    DateTime originalTime = obstacleAppearTimes.Dequeue();
                    obstacleEvent.Invoke(originalTime, captureTime);
                }

                trigger2On = false;
            }
        }

        private bool ContainsObstacle(Rectangle area)
        {
            Bitmap image = CaptureBox(area);
            float range = GetBrightnessRange(image);
            return range > BRIGHTNESS_RANGE_THRESHOLD;
        }

        private float GetBrightnessRange(Bitmap image)
        {
            float min = 0;
            float max = 0;
            float brightness;
            LockBitmap lockBitmap = new LockBitmap(image);
            lockBitmap.LockBits();
            for (int y = 0; y < lockBitmap.Height; y++)
            {
                for (int x = 0; x < lockBitmap.Width; x++)
                {
                    brightness = lockBitmap.GetPixel(x, y).GetBrightness();

                    if (x == 0 && y == 0)
                    {
                        min = brightness;
                        max = brightness;
                        continue;
                    }

                    min = brightness < min ? brightness : min;
                    max = brightness > max ? brightness : max;
                }
            }
            lockBitmap.UnlockBits();

            return max - min;
        }

        private void FocusChrome()
        {
            string chromeWindowTitle = "";
            Process[] processes = Process.GetProcessesByName("chrome");
            for (int i = 0; i < processes.Length; i++)
            {
                if (processes[i].MainWindowTitle != "")
                {
                    chromeWindowTitle = processes[i].MainWindowTitle;
                    break;
                }
            }

            IntPtr chromeHandle = FindWindow(null, chromeWindowTitle);
            if (chromeHandle == IntPtr.Zero)
            {
                MessageBox.Show("Chrome isn't open!");
                return;
            }

            SetForegroundWindow(chromeHandle);
        }



        private Bitmap CaptureBox(Rectangle region)
        {
            Rectangle captureArea = new Rectangle(screen.Bounds.Left + region.Left, screen.Bounds.Top + region.Top, region.Width, region.Height);
            Bitmap captureBitmap = new Bitmap(region.Width, region.Height, PixelFormat.Format32bppArgb);
            Graphics captureGraphics = Graphics.FromImage(captureBitmap);
            captureGraphics.CopyFromScreen(captureArea.Left, captureArea.Top, 0, 0, captureArea.Size);
            return captureBitmap;
        }



        private void Jump()
        {
            SendKeys.Send(" ");
        }



        // ********** UTILITIES ****************



        //private void SampleCapture()
        //{
        //    int millis;
        //    while (true)
        //    {
        //        millis = (int)(DateTime.Now - startTime).TotalMilliseconds;

        //        if (millis > 90000 && (millis % 4000) > 2000)
        //        {
        //            Bitmap image = CaptureImage(705, 225, 600, 30);
        //            //image.Save(@"D:\Jake\Projects\DinoGameChampion\Images\SpeedTests6\" + millis + ".png", ImageFormat.Png);
        //        }
        //        Thread.Sleep(50);
        //    }
        //}

        private Bitmap CaptureImage(int left, int top, int width, int height)
        {
            Screen screen = Screen.FromControl(this);
            Rectangle captureArea = new Rectangle(screen.Bounds.Left + left, screen.Bounds.Top + top, width, height);
            Bitmap captureBitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            Graphics captureGraphics = Graphics.FromImage(captureBitmap);
            Console.WriteLine("Left: " + captureArea.Left + " Top: " + captureArea.Top + " Right: " + captureArea.Right + " Bottom: " + captureArea.Bottom);
            captureGraphics.CopyFromScreen(captureArea.Left, captureArea.Top, 0, 0, captureArea.Size);
            return captureBitmap;
        }

        private Bitmap CaptureScreen()
        {
            Screen screen = Screen.FromControl(this);
            Rectangle captureArea = screen.Bounds;
            Bitmap captureBitmap = new Bitmap(1920, 1080, PixelFormat.Format32bppArgb);
            Graphics captureGraphics = Graphics.FromImage(captureBitmap);
            captureGraphics.CopyFromScreen(captureArea.Left, captureArea.Top, 0, 0, captureArea.Size);
            return captureBitmap;
            //captureBitmap.Save(@"D:\Jake\Projects\DinoGameChampion\Images\test.png", ImageFormat.Png);
        }
    }
}
